package org.anyrtc.whiteboarddemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.anyrtc.whiteboard.http.WhiteBoard;
import org.anyrtc.whiteboard.http.WhiteboardConfig;
import org.anyrtc.whiteboard.http.WihteBoardListener;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements WihteBoardListener {

    WhiteBoard whiteBoard;
    List<String> imageList;
    boolean isTeacher;
    boolean canEdit=false;
    Button editable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        isTeacher =getIntent().getBooleanExtra("isTeacher",true);
        editable=findViewById(R.id.btn_editable);
        whiteBoard=findViewById(R.id.wb_board);
        //注册回调监听
        whiteBoard.setWhiteBoardListener(this);
        //设置图片加载器
        whiteBoard.setImageLoader(new ImageLoader());


        imageList=new ArrayList<>();
        imageList.add(Constans.IMAGE1);
        imageList.add(Constans.IMAGE2);
        imageList.add(Constans.IMAGE3);

        //设置要显示的图片集合
        whiteBoard.displayImageList(imageList);

    }


    /**
     * 验证anyRTC成功，在这回调中初始化画板 board_number 指画板页数编号 1 2 3 4...
     */
    @Override
    public void initAnyRTCSuccess() {
        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                whiteBoard.initWhiteBoard("2018418", "9876543", "#FF0000",isTeacher,isTeacher ? "6666661" : "8888882");//初始化一张画板
                JSONArray boardArray = new JSONArray();
                for (int i = 0; i < imageList.size(); i++) {
                    try {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("board_number", i + 1);
                        jsonObject.put("board_background", imageList.get(i));
                        boardArray.put(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                whiteBoard.initWhiteBoardWithPic("2018418", "9876543", boardArray,isTeacher,isTeacher ? "6666661" : "8888882");
            }
        });

    }

    /**
     * 初始化画板成功
     */
    @Override
    public void initBoardSuccess() {
        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this,"初始化画板成功",Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * 初始话画板失败
     * @param code 错误码
     */
    @Override
    public void initBoardFaild(final int code) {
        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this,"初始化画板失败"+code,Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * 画板是否可编辑
     * @param editable true 可以
     */
    @Override
    public void onBoardEditable(final boolean editable) {
        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.d("whiteboard",editable+"");
                canEdit=editable;
                if (!isTeacher){
                    //设置不可编辑
                    WhiteboardConfig.getInstance().setBoardEditable(editable);
                    Toast.makeText(MainActivity.this,editable ? "可以编辑了" : "不可以编辑了",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * 画板翻页
     * @param currentPage 当前页面
     * @param totlePage 总页面
     */
    @Override
    public void onPageSelect(final int currentPage, final int totlePage) {
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setTitle(currentPage+"/"+totlePage);
                }
            });
    }


    @Override
    protected void onResume() {
        super.onResume();
        setWhiteBoardLayout();
    }

    /**
     * 自己设置白板比例
     */
    private void setWhiteBoardLayout() {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) whiteBoard.getLayoutParams(); //取控件mRlVideoViewLayout当前的布局参数
        final float width = this.getResources().getDisplayMetrics().widthPixels;
        params.height = (int) width;// 强制设置控件的大小
        whiteBoard.setLayoutParams(params); //使设置好的布局参数应用到控件
        whiteBoard.requestLayout();
    }

    public void btnOnclick(View view) {
        switch (view.getId()){
            case R.id.btn_undo:
                whiteBoard.removeLastStroke();
                break;
            case R.id.btn_clean:
                whiteBoard.cleanBoard();
                break;
            case R.id.btn_last:
                whiteBoard.lastPage();
                break;
            case R.id.btn_next:
                whiteBoard.nextPage();
                break;
            case R.id.btn_add:
                whiteBoard.addBoard(0,Constans.IMAGE4);
                break;
            case R.id.btn_del:
                whiteBoard.delBoard(1);
                break;
            case R.id.btn_editable:
                if (isTeacher) {
                    if (canEdit) {
                        whiteBoard.setBoardEnable(false);
                        editable.setText("不可编辑");
                    } else {
                        whiteBoard.setBoardEnable(true);
                        editable.setText("可编辑");
                    }
                    canEdit=!canEdit;
                }else {
                    Toast.makeText(MainActivity.this,"无权限",Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_width:
                WhiteboardConfig.getInstance().setLineWidth(20f);
                break;
            case R.id.btn_color:
                //这里可以设置其他颜色
                WhiteboardConfig.getInstance().setDrawColor("#FF0000");
                break;
            case R.id.btn_shape:
                //这里可设置其他形状
                WhiteboardConfig.getInstance().setDrawType(WhiteboardConfig.Shape.JianTou);
                break;
            case R.id.btn_exit:
                whiteBoard.boardDestory();
                finish();
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            whiteBoard.boardDestory();
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
