package org.anyrtc.whiteboarddemo;

import android.app.Application;

import org.anyrtc.whiteboard.http.WhiteboardConfig;

/**
 * Created by liuxiaozhong on 2018/4/18.
 */

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        //配置开发者信息
        WhiteboardConfig.getInstance().initAnyRTCInfo(Constans.DEVELOPERID, Constans.APPID, Constans.APPKEY, Constans.APPTOKEN);
    }
}
