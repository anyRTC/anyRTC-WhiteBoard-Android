package org.anyrtc.whiteboarddemo;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;

/**
 * Created by liuxiaozhong on 2017-06-09.
 */

public class ImageLoader extends org.anyrtc.whiteboard.imageloader.ImageLoader {
    @Override
    public void displayImage(Context context, Object path, ImageView imageView) {
        RequestManager requestManager= Glide.with(context);
        requestManager.load(path).
               into(imageView);
    }
}
